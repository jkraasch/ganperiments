import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.modules.upsampling import UpsamplingNearest2d
from torch.nn.utils import spectral_norm as SpectralNorm


class SNConv2d(nn.Sequential):
    def __init__(self, *args, **kwargs):
        super(SNConv2d, self).__init__(
            SpectralNorm(nn.Conv2d(*args, **kwargs))
        )


class GenBlock(nn.Module):
    def __init__(self, in_channels, out_channels):
        super(GenBlock, self).__init__()
        self.main = nn.Sequential(
            nn.BatchNorm2d(in_channels),
            nn.ReLU(),
            nn.UpsamplingNearest2d(scale_factor=2),
            SNConv2d(in_channels, out_channels, 3, 1, 1),
            nn.ReLU(),
            SNConv2d(out_channels, out_channels, 3, 1, 1)
        )

        self.sec = nn.Sequential(
            nn.UpsamplingNearest2d(scale_factor=2),
            SNConv2d(in_channels, out_channels, 1, 1)
        )

    def forward(self, x):
        x_res = self.main(x)
        return self.sec(x) + x_res


class DiscBlock(nn.Module):
    def __init__(self, in_channels, out_channels, act=nn.ReLU):
        super(DiscBlock, self).__init__()
        self.diff_c = (in_channels != out_channels)
        self.main = nn.Sequential(
            act(),
            SNConv2d(in_channels, out_channels, 3, 1, 1),
            act(),
            SNConv2d(out_channels, out_channels, 3, 1, 1),
        )
        self.sec = nn.Sequential(
            SNConv2d(in_channels, out_channels, 1, 1)
        )

    def forward(self, x, down=True):
        x_res = self.main(x)
        
        if self.diff_c:
            x = self.sec(x)

        if down:
            sample = nn.AvgPool2d(3, 2, 1)
            return sample(x) + sample(x_res)

        return x + x_res


class DiscOptimBlock(nn.Module):
    def __init__(self, in_channels, out_channels, act=nn.ReLU):
        super(DiscOptimBlock, self).__init__()
        self.main = nn.Sequential(
            SNConv2d(in_channels, out_channels, 3, 1, 1),
            act(),
            SNConv2d(out_channels, out_channels, 3, 1, 1),
            nn.AvgPool2d(3, 2, 1)
        )
        self.sec = nn.Sequential(
            nn.AvgPool2d(3, 2, 1),
            SNConv2d(in_channels, out_channels, 1, 1)
        )

    def forward(self, x):
        x_res = self.main(x)
        return self.sec(x) + x_res


class SelfAttention(nn.Module):
    def __init__(self, in_nc, scale=8):
        super(SelfAttention, self).__init__()
        assert in_nc//scale > 0, "Number of channels can not be scaled done that much"

        self.key_map = nn.Conv2d(in_nc, in_nc//scale, kernel_size=1)
        self.query_map = nn.Conv2d(in_nc, in_nc//scale, kernel_size=1)
        self.h_map = nn.Conv2d(in_nc, in_nc//scale, kernel_size=1)
        self.value_map = nn.Conv2d(in_nc//scale, in_nc, kernel_size=1)

        self.gamma = nn.Parameter(torch.zeros(1))
        self.softmax = nn.Softmax(dim=-1)

    def forward(self, x):
        batchsize, fmaps, height, width = x.size()
        key = self.key_map(x).view(
            batchsize, -1, height*width).permute(0, 2, 1)
        query = self.query_map(x).view(batchsize, -1, height*width)
        h = self.h_map(x).view(batchsize, -1, height*width)

        attention_map = self.softmax(torch.bmm(key, query))
        attention_layer = torch.bmm(h, attention_map)
        attention_layer = attention_layer.view(batchsize, -1, height, width)

        value = self.value_map(attention_layer)
        return (self.gamma * value) + x


class SPADE(nn.Module):
    def __init__(self, norm_fc, norm_nc, mask_nc, ks=3, hidden_nc=128):
        super(SPADE, self).__init__()
        padding_size = ks//2

        self.normalize = norm_fc(norm_nc, affine=False)
        self.shared_conv = nn.Sequential(
            nn.Conv2d(mask_nc, hidden_nc, kernel_size=ks,
                      padding=padding_size),
            nn.ReLU()
        )
        self.scale_conv = nn.Conv2d(
            hidden_nc, norm_nc, kernel_size=ks, padding=padding_size)
        self.bias_conv = nn.Conv2d(
            hidden_nc, norm_nc, kernel_size=ks, padding=padding_size)

    def forward(self, x, segmap):

        # Part 1. generate parameter-free normalized activations
        normalized = self.normalize(x)

        # Part 2. produce scaling and bias conditioned on semantic map
        segmap = F.interpolate(segmap, size=x.size()[2:], mode='nearest')
        actv = self.shared_conv(segmap)
        scale = self.scale_conv(actv)
        bias = self.bias_conv(actv)

        # apply scale and bias
        out = normalized * (1 + scale) + bias

        return out


class RoughSketch(nn.Module):

    def __init__(self, nz):

        super(RoughSketch, self).__init__()
        self.lin1 = nn.Linear(nz, 128*3*3)
        self.out = nn.Sequential(
            nn.ConvTranspose2d(128, 512, 3, 1, 0, 0),
            nn.LeakyReLU(0.2),
            nn.ConvTranspose2d(512, 33, 3, 1, 0, 0))

        self.v = nn.Sequential(
            nn.Linear(nz, 32),
            nn.LeakyReLU(0.2)
        )

        self.R = nn.Sequential(
            nn.ConvTranspose2d(32, 256, 3, 1, 0, 0),
            nn.LeakyReLU(0.2),
            nn.ConvTranspose2d(256, 128, 3, 2, 0, 1)
        )

        self.spade = SPADE(nn.InstanceNorm2d, 128, 32)

    def forward(self, x):
        ao = self.lin1(x).view(-1, 128, 3, 3)
        ao = self.out(ao)
        A, O = (ao[:, 0].view(-1, 1, 7, 7), nn.Softmax2d()(ao[:, 1:]))

        vk = self.v(x).view(-1, 32, 1, 1).expand(-1, 32, 7, 7)
        Rk = self.R(vk*O)

        return self.spade(Rk, A*O)


if __name__ == "__main__":
    sa = SelfAttention(3, scale=1)
    print(sa(torch.randn(16, 3, 64, 64)).shape)

    spade = SPADE(nn.InstanceNorm2d, 64, 3)
    print(spade(torch.randn(16, 64, 64, 64), torch.randn(16, 3, 64, 64)).shape)

    rs = RoughSketch(128)
    print(torch.randn(16, 128).shape)
    print(rs(torch.randn(16, 128)).shape)
