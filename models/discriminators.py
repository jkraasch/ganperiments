import models.gan_utils as gan_utils
import torch.nn as nn
import torch
from torch.nn.utils import spectral_norm as SpectralNorm


class SADiscriminator(nn.Sequential):
    # TODO Conditional BATCH NORM: https://arxiv.org/pdf/1802.05637.pdf
    def __init__(self, nc, ndf, ngpu):
        super(SADiscriminator, self).__init__(
            # input is (nc) x 64 x 64
            SpectralNorm(nn.Conv2d(nc, ndf, 4, 2, 1, bias=False)),
            nn.LeakyReLU(0.2, inplace=True),
            # state size. (ndf) x 32 x 32
            SpectralNorm(nn.Conv2d(ndf, ndf * 2, 4, 2, 1, bias=False)),
            nn.LeakyReLU(0.2, inplace=True),
            # state size. (ndf*2) x 16 x 16
            SpectralNorm(nn.Conv2d(ndf * 2, ndf * 4, 4, 2, 1, bias=False)),
            nn.LeakyReLU(0.2, inplace=True),

            gan_utils.SelfAttention(ndf*4),
            # state size. (ndf*4) x 8 x 8
            SpectralNorm(nn.Conv2d(ndf * 4, ndf * 8, 4, 2, 1, bias=False)),
            nn.LeakyReLU(0.2, inplace=True),
            gan_utils.SelfAttention(ndf*8),
            # state size. (ndf*8) x 4 x 4
            SpectralNorm(nn.Conv2d(ndf * 8, 1, 4, 1, 0, bias=False)),
            nn.Sigmoid()
        )
        self.ngpu = ngpu


class DMDiscriminator(nn.Module):

    def __init__(self, d_feat, c):
        super(DMDiscriminator, self).__init__()

        self.main = nn.Sequential(
            gan_utils.DiscOptimBlock(c, d_feat),
            gan_utils.DiscBlock(d_feat, d_feat*2),
            gan_utils.SelfAttention(d_feat*2),
            gan_utils.DiscBlock(d_feat*2, d_feat*4),
            gan_utils.DiscBlock(d_feat*4, d_feat*8),
            gan_utils.DiscBlock(d_feat*8, d_feat*8),
            nn.ReLU(),
        )
        self.out = SpectralNorm(nn.Linear(d_feat*8, 1))

    def forward(self, x):
        return self.out(torch.sum(self.main(x), dim=(2, 3)))


if __name__ == "__main__":
    gg = DMDiscriminator(d_feat=64, c=3)
    print("Genning")
    print(gg(torch.randn(1, 3, 64, 64)).shape)
