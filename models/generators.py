from models.gan_utils import RoughSketch
import models.gan_utils as gan_utils
import torch.nn as nn
import torch
from torch.nn.utils import spectral_norm as SpectralNorm


class SAGenerator(nn.Sequential):
    # TODO Conditional BATCH NORM: https://arxiv.org/pdf/1802.05637.pdf
    def __init__(self, nz, ngf, nc, ngpu):
        super(SAGenerator, self).__init__(
            SpectralNorm(nn.ConvTranspose2d(
                nz, ngf * 8, 4, 1, 0, bias=False)),
            nn.BatchNorm2d(ngf*8),
            nn.ReLU(True),
            # state size. (ngf*8) x 4 x 4
            SpectralNorm(nn.ConvTranspose2d(
                ngf * 8, ngf * 4, 4, 2, 1, bias=False)),
            nn.BatchNorm2d(ngf*4),
            nn.ReLU(True),
            # state size. (ngf*4) x 8 x 8
            SpectralNorm(nn.ConvTranspose2d(
                ngf * 4, ngf * 2, 4, 2, 1, bias=False)),
            nn.BatchNorm2d(ngf*2),
            nn.ReLU(True),
            gan_utils.SelfAttention(ngf*2),
            # state size. (ngf*2) x 16 x 16
            SpectralNorm(nn.ConvTranspose2d(
                ngf * 2, ngf, 4, 2, 1, bias=False)),
            nn.BatchNorm2d(ngf),
            nn.ReLU(True),
            gan_utils.SelfAttention(ngf),
            # state size. (ngf) x 32 x 32
            nn.ConvTranspose2d(ngf, nc, 4, 2, 1, bias=False),
            nn.Tanh()
            # state size. (nc) x 64 x 64
        )
        self.ngpu = ngpu


class DMGenerator(nn.Module):

    def __init__(self, zs, g_feat, c):
        super(DMGenerator, self).__init__()

        self.g_feat = g_feat
        self.inp = SpectralNorm(nn.Linear(zs, g_feat*8*4*4))
        self.main = nn.Sequential(
            gan_utils.GenBlock(g_feat*8, g_feat*8),
            gan_utils.GenBlock(g_feat*8, g_feat*4),
            gan_utils.GenBlock(g_feat*4, g_feat*2),
            gan_utils.SelfAttention(g_feat*2),
            gan_utils.GenBlock(g_feat*2, g_feat),
            nn.BatchNorm2d(g_feat),
            nn.ReLU(),
            gan_utils.SNConv2d(g_feat, c, 3, 1, 1),
            nn.Tanh()
        )

    def forward(self, x):
        return self.main(self.inp(x.squeeze()).view(-1, self.g_feat*8, 4, 4))


class GameGenerator(nn.Module):

    def __init__(self):

        super(GameGenerator, self).__init__()
        self.rs = RoughSketch(128)
        self.out = nn.Sequential(
            nn.ConvTranspose2d(128, 64, 4, 2, 0, 0),
            nn.LeakyReLU(0.2),
            nn.ConvTranspose2d(64, 32, 4, 2, 0, 0),
            nn.LeakyReLU(0.2),
        )

        self.n = nn.Sequential(
            nn.Conv2d(32, 1, 3, 1),
            nn.Softmax2d()
        )

        self.x = nn.Conv2d(32, 3, 3, 1)

    def forward(self, input):
        input = input.squeeze()
        input = self.rs(input)
        input = self.out(input)

        nu = self.n(input)
        xn = self.x(input)

        return xn*nu


if __name__ == "__main__":
    gg = DMGenerator(128, 64, 3)
    print("Genning")
    print(gg(torch.randn(1, 128, 1, 1)).shape)
